---
title: Lekker kokkerellen
headerimage: "/uploads/IMG_5371.JPG"
description: Kook de lekkerste gerechten in deze uitgeruste keueken!

---
![](/uploads/355353_lsr_2020080635886198645.jpg)

Zelfs in de keuken blijft de gezelligheid doorgaan. Kokerellen en aperitieven gaan hier hand in hand. De grote keuken is voorzien van vaatwasser, amerikaanse koelkast, koffiezet, inductiekookplaat en oven, microgolf... Uiteraard is er een volledig  eetervies  aanwezig en biedt de grote eettafel een stoeltje aan 20 personen. De tafels kunnen naar believen geschikt worden.

![](/uploads/355353_lsr_2020080635884231819.jpg)