---
title: Oergezellige cottage
date: '2018-12-26T06:23:10.000+00:00'
headerimage: "/uploads/IMG_5387.JPG"
description: Goed of slecht weer, in onze cottage zit je goed

---
Hier beleeft u de mooiste avonden. Midden in de natuur, voorzien van alle luxe kan u genieten van een heerlijk drankje en smakelijk barbeque. Plaats genoeg voor alle vrienden en familie. De mogelijkheid is er om de cottage helemaal af  te sluiten en zelfs te verwarmer. Met natuurlijk uw eigen bar en toog.

![](/uploads/IMG_5387.JPG)