---
title: Uw eigen animatieruimte
date: 
headerimage: "/uploads/355353_lsr_2020080635960724158.jpg"
description: Ook binnenshuis kan je en spectaculaire tijd beleven!

---
Zelfs mocht het weer iets minder zijn dan nog niet getreurd. U kan natuurlijk de omliggende steden zoals Bilzen en Alden Biesen, Tongeren en zijn Gallo Romeins museum, het bruisende Maastricht en zoveel meer bezoeken. Maar in onze ruime, vrijstaande kindvriendelijke villa is een brede waaier aan animatie voorzien voor jong en oud. U kan in een ruime overdekte chillroom genieten van onze pooltafel en kickertafel. Allemaal inclusief. Bovendien is er ook een  oergezellige overdekte cottage met verwarmers om nog lang na te genieten van de bbq. U kan zelfs beschikken over een eigen bar met gevulde frigo’s met o.a veel streekbiertjes. Geniet ervan aan onze huistoog.  
Buiten is er een volledig omheinde speelweide voorzien met glijbaan, schommels, vikingkub...