---
title: Zoete dromen
date: 
headerimage: "/uploads/355353_lsr_2020080635930087688.jpg"
description: Slaap als een roosje in deze comfortabele slaapkamers

---
**Flower bedroom**

Masterbedroom 1. Gelijkvloers(36m2):

Tweepersoonsboxspring 1.80cm + comfortabel zetelbed

Genieten van een zalige nachtrust doe je hier.

![](/uploads/IMG_5307.JPG)

**Romantic bedroom**

Slaapkamer 2. Eerste verdieping(16m2):

Tweepersoonsbed 1.80cm

![](/uploads/IMG_5328.JPG)

**Happy bedroom**

Slaapkamer 3. Eerste verdieping(12m2):

Tweepersoonsboxspring 1.80cm + leuk stapelbed.

![](/uploads/IMG_5331.JPG)

**Trio Attic bedroom**

Slaapkamer 4. Tweede verdieping(12m2):

3 stijlvolle éénpersoonsbedden

![](/uploads/355353_lsr_2020080635922884239.jpg)

**Attic bedroom**

Slaapkamer 5. Tweede verdieping(16m2):

Tweepersoons boxspring1.80 cm. Op aanvraag aan te passen naar éénpersoonsbedden.

![](/uploads/355353_lsr_2020080635930087688.jpg)

**Red bedroom**

Slaapkamer 6. Gelijkvloers (12m2):

Tweepersoonsboxspring 1.80 cm. Op aanvraag aan te passen naar éénpersoonsbedden.

![](/uploads/355353_lsr_2020080635942253845.jpg)

**Green bedroom**

Slaapkamer 7. Gelijkvloers(14m2):

Tweepersoonsboxspring 1.80 cm + stapelbed

![](/uploads/355353_lsr_2020080635947001984.jpg)
