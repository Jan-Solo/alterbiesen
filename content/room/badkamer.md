---
title: Uw Badkamers
date: 
headerimage: "/uploads/IMG_5374.JPG"
description: Wordt helemaal zen in onze relaxerende badkamers

---
U kan gebruik maken van maar liefst vier badkamers. Op de eerste verdieping vind u een luxueuze badkamer voorzien van wastafel, inloopdouche en toilet.

![](/uploads/355353_lsr_2020080635915508894.jpg)

Op het gelijkvloers is er een badkamer voorzien met bubbelbad en  lavabo.

![](/uploads/IMG_5374.JPG)

Apart toilet.

De derde badkamer is voorzien van een brede wastafel en een inloopdouche. De vierde badkamer is  hieraan gelijk maar beschikt nog over een extra toilet.